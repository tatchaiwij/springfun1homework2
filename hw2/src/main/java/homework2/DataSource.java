package homework2;

public class DataSource {
    public String driverClassName;
    public String url;
    public String username;
    public String Password;


    public DataSource() {
    }

    public DataSource(String driverClassName, String url, String username, String Password) {
        this.driverClassName = driverClassName;
        this.url = url;
        this.username = username;
        this.Password = Password;
    }

    public String getDriverClassName() {
        return this.driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public DataSource driverClassName(String driverClassName) {
        setDriverClassName(driverClassName);
        return this;
    }

    public DataSource url(String url) {
        setUrl(url);
        return this;
    }

    public DataSource username(String username) {
        setUsername(username);
        return this;
    }

    public DataSource Password(String Password) {
        setPassword(Password);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " driverClassName='" + getDriverClassName() + "'" +
            ", url='" + getUrl() + "'" +
            ", username='" + getUsername() + "'" +
            ", Password='" + getPassword() + "'" +
            "}";
    }

}
