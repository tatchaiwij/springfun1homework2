package homework2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

    public static void main(String[] args){
        try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml")){
            DataSource datasource = (DataSource)context.getBean("DataSource");
            try {
                Class.forName(datasource.driverClassName);
            }catch(ClassNotFoundException e){}
            try{
                Connection conn = DriverManager.getConnection(datasource.url, datasource.username, datasource.Password);
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT name FROM testtable");
                while (rs.next()) {
                    System.out.print("Name: " + rs.getString("name"));
                }
            }catch(SQLException e){}
        }
    }
}
